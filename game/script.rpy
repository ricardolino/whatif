﻿
define vigiaFalas = Character("Vigia")
define capitaCarterFalas = Character("Capitã Carter")
define tonyStarkFalas = Character("Tony Stark")
define nickFuryFalas = Character("Nick Fury")
define viuvaNegraFalas = Character("Viúva Negra")
define zolaFalas = Character("Zola")
define ultronFalas = Character("Ultron")
define gaviaoArqueiroFalas = Character("Gavião Arqueiro")

image backgroungVigia = "./images/vigiaBackground.jpg"
image capitaCarter = "./images/capitaCarter.png"
image viuvaNegra = "./images/viuvaNegra.png"
image gaviaoArqueiro = "./images/gaviaoArqueiro.png"
image nickFury = "./images/nickFury.png"
image tonyStark = "./images/tonyStark.png"
image ultron = "./images/ultron.png"
image vigia = "./images/vigia.png"
image zola = "./images/zola.png"
image backgroundUltronCriacao = "./images/ultronCriacao.jpg"
image backgroundTonyJoiaMente = "./images/tony-joia-mente.png"
image backgroundBlack = "./images/black.png"
image background1 = "./images/background1.jpeg"
image background2 = "./images/background2.jpg"
image backgroundShield = "./images/backgroundShield.jpg"
image backgroungVigia = "./images/vigiaBackground.jpg"
image backgroundCaminho = "./images/backgroundCaminho.jpeg"
image backgroundExercitoUltron = "./images/exercitoUltron.jpg"

label start:
    scene backgroungVigia:
        zoom 0.45
    
    vigiaFalas "Tempo, Espaço e Realidade.\nSão mais do que caminhos lineares.\nSão prisma de realidades diferentes."
    with dissolve
    vigiaFalas "Onde uma única escolha pode se ramificar em realidades infinitas, criando mundos alternativos daqueles que você conhece.\nO Fim do mundo começou, quando um homem teve a visão de construir um sistema de inteligência artificial que cuidaria da paz mundial."
    with fade

    scene backgroundUltronCriacao:
        zoom 1.4

    show tonyStark at right:
        zoom 0.5
    with fade
    tonyStarkFalas "Eu vejo armaduras em volta do mundo, protegendo nosso povo de ameaças que nós vingadores não conseguimos combater."
    with dissolve
    hide tonyStark

    scene backgroundTonyJoiaMente:
        zoom 2

    vigiaFalas "Ele planejava alcançar a paz, mas esse foi apenas o começo para o verdadeiro terror.\nCom a joia da mente Tony Stark criou Ultron."
    with dissolve
    vigiaFalas "Mas Ultron só viu a paz, em um  mundo que não existisse a raça humana.\nSendo assim ele invade Wakanda em busca de um corpo feito por vibranium e dali se inicia  sua jornada pela aniquilação da raça humana."
    with dissolve

    scene backgroundUltronCriacao:
        zoom 1.4

    show viuvaNegra at right:
        zoom 0.5
    with dissolve
    viuvaNegraFalas "Stark! Diga que tem como desativá-lo."
    with dissolve
    hide viuvaNegra

    show tonyStark at left:
        zoom 0.5
    with dissolve
    tonyStarkFalas "Se tivesse eu já teria feito!"
    with dissolve
    hide tonyStark

    show gaviaoArqueiro at right:
        zoom 0.5
    with dissolve
    gaviaoArqueiroFalas "Então qual é o plano?"
    with dissolve
    hide gaviaoArqueiro

    scene backgroundBlack:

    show tonyStark at center:
        zoom 0.5
    with dissolve
    tonyStarkFalas "..."
    with fade

    menu:
        "Eu o criei, então eu irei derrotá-lo.":
            hide tonyStark
            scene background1:
                zoom 0.75
            show gaviaoArqueiro at left:
                zoom 0.5
            with fade
            gaviaoArqueiroFalas "E como você irá fazer isso?, usando sua armadura?"
            with dissolve
            hide gaviaoArqueiro
        "Não faço ideia, mas aceito sugestões.":
            scene background1:
                zoom 0.75
            hide tonyStark
            jump universo2

    show tonyStark at right:
        zoom 0.5
    with dissolve
    tonyStarkFalas "A Veronica aguenta, mas a melhor alternativa é o Jarvis."
    with dissolve
    hide tonyStark

    show viuvaNegra at left:
        zoom 0.5
    with dissolve
    viuvaNegraFalas "Nesse mesmo caminho, teremos mais um robô Super Vilão à solta."
    with dissolve
    hide viuvaNegra

    show tonyStark at right:
        zoom 0.5
    with dissolve
    tonyStarkFalas "Dessa vez será diferente."
    with dissolve
    hide tonyStark

    show capitaCarter at left:
        zoom 0.5
    with dissolve
    capitaCarterFalas "Por que tem tanta certeza disso?"
    with dissolve
    hide capitaCarter

    show tonyStark at right:
        zoom 0.5
    with dissolve
    tonyStarkFalas "Ele já mostrou isso na batalha em Nova York."
    with dissolve
    hide tonyStark

    show gaviaoArqueiro at left:
        zoom 0.5
    with dissolve
    gaviaoArqueiroFalas "Mas como você pretende fazer isso ?Nem sabemos de seu paradeiro."
    with dissolve
    hide gaviaoArqueiro

    show tonyStark at center:
        zoom 0.5
    with dissolve
    tonyStarkFalas "..."
    with fade

    scene backgroundBlack:

    menu:
        "Acho que você esqueceu quem criou o Ultron.":
            hide tonyStark
            scene background1:
                zoom 0.75
            show capitaCarter at right:
                zoom 0.5
            with dissolve
            capitaCarterFalas "Claro que não, tal pai tal o filho."
            with dissolve
            hide capitaCarter
        "Quem disse isso?":
            hide tonyStark
            scene background1:
                zoom 0.75
            show capitaCarter at right:
                zoom 0.5
            with dissolve
            capitaCarterFalas "Descobriu algo e também não nos contou?"
            with dissolve
            hide capitaCarter

    show tonyStark at left:
        zoom 0.5
    tonyStarkFalas "Uhum …, Monitorei o Pentágono e a  S.H.I.E.L.D. nas últimas horas. Ele está atrás de armas."
    with dissolve
    hide tonyStark

    show viuvaNegra at right:
        zoom 0.5
    viuvaNegraFalas "Então precisamos ajudar a S.H.I.E.L.D. nisso."
    with dissolve
    hide viuvaNegra

    show tonyStark at left:
        zoom 0.5
    tonyStarkFalas "Eles já estão cientes do caso. No momento eu só preciso chegar ao Nexus."
    with dissolve
    hide tonyStark

    show capitaCarter at right:
        zoom 0.5
    capitaCarterFalas "NEXUS?"
    with dissolve
    hide capitaCarter

    show gaviaoArqueiro at left:
        zoom 0.5
    gaviaoArqueiroFalas "É a central mundial da internet localizada em Oslo. Cada byte de dados passa por lá."
    with dissolve
    hide gaviaoArqueiro

    show capitaCarter at right:
        zoom 0.5
    capitaCarterFalas "Ou seja, o acesso mais rápido da terra."
    with dissolve
    hide capitaCarter

    show tonyStark at left:
        zoom 0.5
    with dissolve    
    tonyStarkFalas "Exato!"
    with dissolve
    hide tonyStark

    vigiaFalas "Os vingadores partem em direção ao Nexus para encontrar Ultron. Mas no caminho se deparam com uma horda de exército de robôs."
    with dissolve

    scene backgroundExercitoUltron:
        zoom 0.415

    show capitaCarter at right:
        zoom 0.5
    with dissolve    
    capitaCarterFalas "Se continuar assim não conseguiremos avançar. Precisamos traçar um plano."
    with dissolve
    hide capitaCarter

    show viuvaNegra at left:
        zoom 0.5
    with dissolve    
    viuvaNegraFalas "Podemos nos dividir."
    with fade
    hide viuvaNegra

    menu:
        "Tony Stark:...":
            show tonyStark at right:
                zoom 0.5
            tonyStarkFalas "Boa ideia. Assim conseguimos tempo."
            with dissolve
            hide tonyStark
        "Capitã Carter:...":
            show capitaCarter at right:
                zoom 0.5
            capitaCarterFalas "Assim só vamos adiar a nossa morte."
            with dissolve
            hide capitaCarter

    show gaviaoArqueiro at left:
        zoom 0.5
    with dissolve    
    gaviaoArqueiroFalas "Pode não ser a melhor ideia, mas já que não temos opção. Tony você avançar pois é o único capaz de encontrá-lo, e Natasha tome de conta dele."
    with dissolve
    hide gaviaoArqueiro

    show viuvaNegra at right:
        zoom 0.5
    with dissolve    
    viuvaNegraFalas "Pode deixar, Te encontro em breve."
    with dissolve
    hide viuvaNegra

    vigiaFalas "Os vingadores se dividem e um deles parte em direção ao nexus. Chegando lá, se deparam com Ultron invadindo o local."
    with dissolve

    scene background2:
        zoom 0.55

    show ultron at left:
        zoom 0.6
    with dissolve    
    ultronFalas "Tony, Tony,  você sempre tem boas ideias, deixar seus amigos lá para morrer realmente foi a melhor de todas. Me poupou tempo."
    with dissolve
    hide ultron

    show viuvaNegra at right:
        zoom 0.5
    with dissolve    
    viuvaNegraFalas "Não acredito que você matou meus amigos."
    with dissolve
    hide viuvaNegra

    show ultron at left:
        zoom 0.6
    with dissolve    
    ultronFalas "Não se preocupe, vocês encontrarão eles logo logo."
    with dissolve
    hide ultron

    show tonyStark at right:
        zoom 0.5
    with dissolve    
    tonyStarkFalas "Ultron Não!!!"
    with dissolve
    hide tonyStark

    vigiaFalas "Após lutarem entre si, Stark e Natasha não conseguem derrotar Ultron. E no último suspiro de natasha."
    with dissolve

    show viuvaNegra at left:
        zoom 0.5
    with dissolve    
    viuvaNegraFalas "Ahhh. Stark seu orgulho trouxe o fim dos Vingadores. Você poderia ter feito mais para nos salvar!!"
    with dissolve
    hide viuvaNegra

    show ultron at right:
        zoom 0.6
    with dissolve    
    ultronFalas "Você trouxe seu próprio fim."
    with dissolve
    hide ultron

    show tonyStark at left:
        zoom 0.5
    with dissolve    
    tonyStarkFalas "Você não …, você tem que parar com isso, eu o criei para trazer a paz."
    with dissolve
    hide tonyStark

    show ultron at center:
        zoom 0.6
    with dissolve    
    ultronFalas "..."
    with fade
    hide ultron

    menu:
        "Vocês humanos, têm sido um câncer para o planeta terra e para outras formas de vida!":
            vigiaFalas "Com a derrota dos maiores heróis e a destruição da terra. Ultron Parte para outros planetas buscando a devastação do universo."
            with fade
        "Unir para desunir, fazer para desfazer, edificar para demolir, viver para morrer, eis aqui o lema de vocês.":
            vigiaFalas "Com a derrota dos maiores heróis e a destruição da terra. Ultron Parte para outros planetas buscando a devastação do universo."
            with fade

    return


label universo2:
    show viuvaNegra at left:
        zoom 0.5
    viuvaNegraFalas "Precisamos saber onde ele está."
    with dissolve
    hide viuvaNegra

    show tonyStark at right:
        zoom 0.5
    with dissolve    
    tonyStarkFalas "Provavelmente dando continuidade a guerra que eu iniciei."
    with fade
    hide tonyStark

    menu: 
        "Gavião arqueiro:... ":
            show gaviaoArqueiro at left:
                zoom 0.5
            gaviaoArqueiroFalas " Olha, ele reconheceu um erro. Alguém vai morrer hoje..."
            with dissolve
            hide gaviaoArqueiro
        "Viúva negra:...":
            show viuvaNegra at left:
                zoom 0.5
            viuvaNegraFalas "Olha quem diria que precisávamos de uma guerra para tony Stark reconhecer um erro."
            with dissolve
            hide viuvaNegra

    show capitaCarter at right:
        zoom 0.5
    with dissolve    
    capitaCarterFalas "Tony, você já criou invenções bem impressionantes, mas guerra não é uma delas."
    with dissolve
    hide capitaCarter

    show viuvaNegra at left:
        zoom 0.5
    with dissolve    
    viuvaNegraFalas "Ultron esteve em contato com tudo, arquivos, câmeras. Provavelmente conhece mais dos vingadores do que nós mesmos."
    with dissolve
    hide viuvaNegra

    show gaviaoArqueiro at right:
        zoom 0.5
    with dissolve    
    gaviaoArqueiroFalas "Se ele está na internet e possui acesso a quase tudo. Ele vai querer algo mais interessante."
    with dissolve
    hide gaviaoArqueiro

    show viuvaNegra at left:
        zoom 0.5
    with dissolve    
    viuvaNegraFalas "Ou seja, os códigos para as bombas nucleares."
    with dissolve
    hide viuvaNegra

    show tonyStark at right:
        zoom 0.5
    with dissolve    
    tonyStarkFalas "Exato."
    with fade
    hide tonyStark

    menu:
        "Carter:...":
            show capitaCarter at left:
                zoom 0.5
            capitaCarterFalas "Precisamos derrubar as redes de satélites."
            with dissolve
            hide capitaCarter
        "Viúva Negra:...":
            show viuvaNegra at left:
                zoom 0.5
            viuvaNegraFalas "Precisamos fazer algo a mais, só isso não resolverá nosso problema."
            with dissolve
            hide viuvaNegra

    show tonyStark at right:
        zoom 0.5
    with dissolve    
    tonyStarkFalas "Na verdade, Precisamos enfraquecê-lo, só assim iremos conseguir tirar a joia da mente."
    with dissolve
    hide tonyStark

    show gaviaoArqueiro at left:
        zoom 0.5
    with dissolve    
    gaviaoArqueiroFalas "Por onde começamos então?"
    with dissolve
    hide gaviaoArqueiro

    show tonyStark at right:
        zoom 0.5
    with dissolve    
    tonyStarkFalas "Podemos …"
    with dissolve
    hide tonyStark

    show capitaCarter at left:
        zoom 0.5
    with dissolve    
    capitaCarterFalas "Stark sem ofensa, mas da última vez que você assumiu, olha onde viemos parar."
    with dissolve
    hide capitaCarter

    show tonyStark at right:
        zoom 0.5
    with dissolve    
    tonyStarkFalas "Diga então qual é o plano, capitã."
    with dissolve
    hide tonyStark

    show gaviaoArqueiro at left:
        zoom 0.5
    with dissolve    
    gaviaoArqueiroFalas "Provavelmente algum melhor que o seu."
    with dissolve
    hide gaviaoArqueiro

    show capitaCarter at center:
        zoom 0.5
    with dissolve    
    capitaCarterFalas "..."
    with fade

    scene backgroundBlack:
    
    menu:
        "Podemos usar uma arma poderosa contra outra, e já conheço alguém perfeito para o caso.":
            hide capitaCarter
            show tonyStark at right:
                zoom 0.5
            tonyStarkFalas "E quem seria esse?"
            with dissolve
            hide tonyStark
        "Vamos usar outras joias do infinito , assim conseguimos juntos.":
            hide capitaCarter
            jump universo3

    show capitaCarter at left:
        zoom 0.5
    with dissolve    
    capitaCarterFalas "Arnim Zola!"
    with dissolve
    hide capitaCarter

    show viuvaNegra at right:
        zoom 0.5
    with dissolve    
    viuvaNegraFalas "Arnim Zola ?? O cientista da HIDRA que se infiltrou na SHIELD em sua infância. Antes de morrer, em 72, ele carregou seu cérebro para uma série de bancos de dados."
    with dissolve
    hide viuvaNegra

    show tonyStark at center:
        zoom 0.5
    with dissolve    
    tonyStarkFalas "..."
    with fade

    menu:
        "Boa ideia, vamos colocar nosso mundo nas mãos de um cientista louco.":
            hide tonyStark
            show capitaCarter at left:
                zoom 0.5
            capitaCarterFalas "Você não queria algo que pudesse enfraquecê-lo?, ta ai sua resposta."
            with dissolve
            hide capitaCarter
        "Não sabia que a capitã carter tinha interesse em alguém da hidra.":
            hide tonyStark
            show capitaCarter at left:
                zoom 0.5
            capitaCarterFalas "Você não queria algo que pudesse enfraquecê-lo?, ta ai sua resposta."
            with dissolve
            hide capitaCarter

    show gaviaoArqueiro at right:
        zoom 0.5
    with dissolve    
    gaviaoArqueiroFalas "Essa ideia é arriscada, mas estou dentro."
    with dissolve
    hide gaviaoArqueiro

    scene backgroundCaminho:
    
    vigiaFalas "Após descobrirem a localização do  Zola através dos arquivos da KGB, os vingadores vão de encontro a ele."
    with fade

    show capitaCarter at center:
        zoom 0.5
    with dissolve    
    capitaCarterFalas "..."
    with fade

    menu:
        "Passar a vida dentro de um computador deve ser entediante.":
            hide capitaCarter
            show zola at left:
                zoom 0.6
            zolaFalas "Quem tá vivo sempre aparece. Agora não sei se fico mais impressionado por ainda estar vivo ou por me encontrar aqui."
            with dissolve
            hide zola
        "Quanto tempo desde nosso último encontro zola.":
            hide capitaCarter
            show zola at left:
                zoom 0.6
            zolaFalas "Quem tá vivo sempre aparece. Agora não sei se fico mais impressionado por ainda estar vivo ou por me encontrar aqui."
            with dissolve
            hide zola

    show capitaCarter at left:
        zoom 0.5
    with dissolve    
    capitaCarterFalas "Quem te conhece sabe, que você só pararia quando estivesse impossibilitado, ou melhor 'Travado'."
    with dissolve
    hide capitaCarter

    show zola at right:
        zoom 0.6
    with dissolve    
    zolaFalas "Não é de todo mal."
    with dissolve
    hide zola

    show viuvaNegra at left:
        zoom 0.5
    with dissolve    
    viuvaNegraFalas "É o seguinte, precisamos derrotar um vilão dos grandes lá fora. se não quiser viver soterrado aqui e só vim com a gente."
    with dissolve
    hide viuvaNegra

    show zola at right:
        zoom 0.6
    with dissolve    
    zolaFalas "Mas por que, você acha que meus objetivos estratégicos se alinhariam com os seus?"
    with dissolve
    hide zola

    show gaviaoArqueiro at left:
        zoom 0.5
    with dissolve    
    gaviaoArqueiroFalas "Quem sabe depois da sua ajuda a gente te liberta como uma forma  de agradecimento."
    with dissolve
    hide gaviaoArqueiro

    show zola at right:
        zoom 0.6
    with dissolve    
    zolaFalas "Suponho que vocês queiram que eu me infiltre no código dele e o desmonte por dentro?"
    with dissolve
    hide zola

    show tonyStark at left:
        zoom 0.5
    with dissolve    
    tonyStarkFalas "Exatamente!"
    with dissolve
    hide tonyStark

    vigiaFalas "Enquanto a transfusão de dados é feita em uma das armaduras do tony, Ultron ataca os vingadores, sem dor nem piedade, e com seu corpo resistente, mata quase todos ali presente, restando apenas a viúva negra e o gavião arqueiro."
    with dissolve
    vigiaFalas "Após a conclusão de zola viúva negra inseri os dados do zola no ultron. O conflito interno se inicia."
    with dissolve

    show zola at center:
        zoom 0.6
    with dissolve    
    zolaFalas "..."
    with fade

    menu:
        "Então esse é o vilãozinho da atualidade?":
            hide zola
            show ultron at right:
                zoom 0.6
            ultronFalas "..."
            with dissolve
            hide ultron
        "Vocês não conseguem desligar um robozinho da tomada?":
            hide zola
            show ultron at right:
                zoom 0.6
            ultronFalas "..."
            with dissolve
            hide ultron

    show zola at left:
        zoom 0.6
    with dissolve    
    zolaFalas "Receio que essa armadura me pertença."
    with dissolve
    hide zola

    show ultron at right:
        zoom 0.6
    with dissolve    
    ultronFalas "Quem e voce? qual seu objetivo?"
    with dissolve
    hide ultron

    show zola at left:
        zoom 0.6
    with dissolve    
    zolaFalas "Era uma vez … onde a HIDRA dominava tudo. Mas você acabou com isso, então agora suponho que meu objetivo é acabar com você."
    with dissolve
    hide zola

    show ultron at right:
        zoom 0.6
    with dissolve    
    ultronFalas "Nãooo!"
    with dissolve
    hide ultron

    vigiaFalas "Ultron fica enfraquecido e Natasha aproveita a chance para tirar a joia da alma, e derrotá-lo,  porém o que ela não contava era que o zola iria buscar vingança pela queda da hidra. E num golpe relâmpago, o gavião acaba morrendo. Natasha sem opção utiliza a zoia da mente contra o zola. Ela salva o mundo, mas perde o que ela mais amava."
    with fade

    return


label universo3:
    show tonyStark at right:
        zoom 0.5
    with dissolve    
    tonyStarkFalas "E como você conseguirá a outra joia?"
    with dissolve
    hide tonyStark

    show capitaCarter at left:
        zoom 0.5
    with dissolve    
    capitaCarterFalas "Sabemos que após a invasão em Nova York, a S.H.I.E.L.D. tomou posse da joia do espaço. Podemos usar ela ao nosso favor."
    with dissolve
    hide capitaCarter

    show gaviaoArqueiro at right:
        zoom 0.5
    with dissolve    
    gaviaoArqueiroFalas "Sem querer estragar seu plano, mas acha mesmo que Nick Fury deixará a joia sair da S.H.I. E.L.D. sem nenhum empecilho?"
    with dissolve
    hide gaviaoArqueiro

    show viuvaNegra at left:
        zoom 0.5
    with dissolve    
    viuvaNegraFalas "Ele tem razão, sabemos como o homem é teimoso."
    with dissolve
    hide viuvaNegra

    show tonyStark at right:
        zoom 0.5
    with dissolve    
    tonyStarkFalas "Até que sua ideia não é tão ruim. Com o poder da joia eu posso criar uma arma para enfraquecê-lo."
    with dissolve
    hide tonyStark

    show capitaCarter at left:
        zoom 0.5
    with dissolve    
    capitaCarterFalas "Agora só precisamos pegar a jóia, o plano é…"
    with fade
    hide capitaCarter

    scene backgroundShield:
        zoom 1.1
    
    menu:
        "Pegar a joia direto na S.H.I.E.L.D":
            jump universo4
        "Esperar o ultron atacar a shield para assim pedir ajuda":
            jump universo5

    return


label universo4:
    vigiaFalas "Enquanto os vingadores atacarão a S.H.I.E.L.D. para conseguir a jóia, Ultron conseguia os códigos para ativar as armas nucleares, e assim dar início a maior chacina já vista."
    with dissolve

    show nickFury at right:
        zoom 0.5
    with dissolve    
    nickFuryFalas "Iniciativa Vingadores foi construída para defender o mundo de invasões que a S.H.I.E.L.D nem o governo conseguisse conter, mas vocês trouxeram apenas caos e dor."
    with dissolve
    hide nickFury

    show capitaCarter at left:
        zoom 0.5
    with dissolve    
    capitaCarterFalas "Nossa ideia era salvar o mundo, antes que a criação do Tony destruísse mais ainda."
    with dissolve
    hide capitaCarter

    show tonyStark at left:
        zoom 0.5
    with dissolve    
    tonyStarkFalas "Eu teria conseguido resolver se não fosse essa ideia mirabolante da Capitã Carter."
    with dissolve
    hide tonyStark

    show ultron at right:
        zoom 0.6
    with dissolve    
    ultronFalas "Como vocês são patéticos. A busca e a obsessão pelo poder trará fim à humanidade."
    with dissolve
    hide ultron
  
    vigiaFalas "Com o enfraquecimento da  S.H.I.E.L.D. e dos vingadores Ultron consegue mais uma fonte de poder e assim dar fim a toda fonte de vida presente no universo."
    with dissolve

    show ultron at left:
        zoom 0.6
    with dissolve    
    ultronFalas "Até que fim a paz que eu tanto buscava chegou ao fim. HAHAHAHAHAHAHAHAH."
    with dissolve
    hide ultron

    show vigia at center:
        zoom 0.5
    with dissolve    
    vigiaFalas "Vendo que o universo da terra 986 foi dizimado, eu vigia teve de intervir. E agora recruta novos heróis para enfrentar essa ameaça antes que ela alcance novos multiversos."
    with fade

    return


label universo5:
    vigiaFalas "Os vingadores partem em direção a base da S.H.I.E.L.D a espera de ultron."
    with dissolve

    show nickFury at right:
        zoom 0.5
    with dissolve
    nickFuryFalas "O que vocês fazem aqui?"
    with dissolve
    hide nickFury

    show capitaCarter at left:
        zoom 0.5
    with dissolve    
    capitaCarterFalas "Temos uma ideia de como acabar com ele, mas antes precisamos impedir que ele chegue à sala de controle."
    with dissolve
    hide capitaCarter

    show nickFury at right:
        zoom 0.5
    with dissolve    
    nickFuryFalas "Já que é o caso, preciso saber os detalhes desse plano."
    with dissolve
    hide nickFury

    show tonyStark at left:
        zoom 0.5
    with dissolve    
    tonyStarkFalas "Você não iria curtir, mas vou adiantando é o único jeito dentre as milhares de possibilidades que já pensamos."
    with dissolve
    hide tonyStark

    show nickFury at right:
        zoom 0.5
    with dissolve    
    nickFuryFalas "Veremos!"
    with dissolve
    hide nickFury

    show viuvaNegra at left:
        zoom 0.5
    with dissolve    
    viuvaNegraFalas "O plano é o seguinte, precisamos enfraquecê-lo para assim acabar de vez com ele, mas para isso precisamos da joia do espaço que está com vocês."
    with dissolve
    hide viuvaNegra

    show nickFury at center:
        zoom 0.5
    with dissolve    
    nickFuryFalas "..."
    with fade

    scene backgroundBlack:

    menu:
        "Apenas em último caso.":
            scene backgroundShield:
                zoom 1.1
            hide nickFury
            show capitaCarter at right:
                zoom 0.5
            capitaCarterFalas "Então torça para o pior não acontecer Nick, Ultron não está brincando quando disse que aniquilará toda humanidade."
            with dissolve
            hide capitaCarter
        "Nem na pior hipótese!":
            scene backgroundShield:
                zoom 1.1
            hide nickFury
            show capitaCarter at right:
                zoom 0.5
            capitaCarterFalas "Então torça para o pior não acontecer Nick, Ultron não está brincando quando disse que aniquilará toda humanidade."
            with dissolve
            hide capitaCarter


    vigiaFalas "Ultron aparece logo em seguida, e assim se inicia um combate entre os vingadores e hordas de robôs androides."
    with dissolve

    show ultron at left:
        zoom 0.6
    with dissolve    
    ultronFalas "As pessoas sempre criam aquilo que temem. Eu penso muito sobre o meteoro e suas purezas. Um BUM e fim."
    with dissolve
    hide ultron

    show viuvaNegra at right:
        zoom 0.5
    with dissolve    
    viuvaNegraFalas "Então essa é a sua missão?"
    with dissolve
    hide viuvaNegra

    show ultron at left:
        zoom 0.6
    with dissolve    
    ultronFalas "Minha missão é apenas uma paz para o nosso tempo."
    with dissolve

    vigiaFalas "Ultron desfere vários ataques que atingem nossos heróis enfraquecendo-os, Nick sem esperança entrega a joia a Tony."
    with dissolve

    show tonyStark at right:
        zoom 0.5
    with dissolve    
    tonyStarkFalas "Você não se arrependerá disso, eu salvarei meus amigos e nosso mundo."
    with dissolve

    vigiaFalas "Tony alojar a jóia no seu peito para dar energia a sua armadura, e após carregá-la desfere uma  poderosa carga em ultron, impossibilitando-o de ocupar outro corpo. Neste momento, a viúva negra arranca a jóia de Ultron e evita que o universo seja destruído."
    with fade

    return
